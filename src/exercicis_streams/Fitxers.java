package exercicis_streams;

import Varies.Data;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDateTime;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Fitxers {
    public static void menu1() throws IOException {
        BufferedReader brA = null;
        BufferedReader brB = null;
        String path1 = "a.txt";
        String path2 = "b.txt";
        int numLiniesDif;
        String lineaA;
        String lineaB;
        int i;

        try {
            brA = new BufferedReader(new FileReader(new File(path1)));
            brB = new BufferedReader(new FileReader(new File(path2)));
            numLiniesDif = 0;
            lineaA = brA.readLine();
            lineaB = brB.readLine();
            i = 1;
            while (lineaA != null && lineaB != null) {
                if (!lineaA.equals(lineaB)) {
                    System.out.println("Falla la línia "+ i);
                    System.out.println("Linia de " + path1 + ": " + lineaA);
                    System.out.println("Linia de " + path2 + ": " + lineaB);
                    numLiniesDif++;
                }
                lineaA = brA.readLine();
                lineaB = brB.readLine();
                i++;
            }
            if (lineaA != null) {
                while (lineaA != null) {
                    System.out.println("Falla la línia " + i);
                    System.out.println("Linia de " + path1 + ": " + lineaA);
                    System.out.println("Linia de " + path2 + ": NULL");

                    numLiniesDif++;
                    lineaA = brA.readLine();
                    i++;
                }
            } else {
                if (lineaB != null) {
                    while (lineaB != null) {
                        System.out.println("Falla la línia " + i);
                        System.out.println("Linia de " + path1 + ": NULL");
                        System.out.println("Linia de " + path2 + ": " + lineaB);

                        numLiniesDif++;
                        lineaB = brB.readLine();
                        i++;
                    }
                }
            }

            System.out.println("Número de línies diferents: " + numLiniesDif);
            System.out.println("FI DE LA COMPARACIÓ");
        } catch (FileNotFoundException e) {
            System.out.println("Excepció provocada pel \"new FileReader()\".");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Excepció provocada pel \"buffer.readLine()\".");
            e.printStackTrace();
        } finally {
            brA.close();
            brB.close();
        }
    }

    public static void menu2() throws IOException {
        List<String> arxius = new ArrayList<String>();
        FileReader fileReader;
        int numLinies;
        int numCaracters;
        BufferedReader bufferedReader = null;
        String linia;

        arxius.add("a.txt");
        arxius.add("b.txt");

        for (String arxiu : arxius) {
            File f = new File(arxiu);
            numLinies = 0;
            numCaracters = 0;

            try {
                fileReader = new FileReader(f);
                bufferedReader = new BufferedReader(fileReader);

                while ((linia = bufferedReader.readLine()) != null) {
                    numCaracters += linia.length();
                    numLinies++;
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                bufferedReader.close();
            }
            System.out.println("L'arxiu " + f.getName() + " té " + numLinies + " linies i " + numCaracters + " caracters." );
        }
    }

    public static void menu3() throws IOException {
        List<String> arxius = new ArrayList<String>();
        FileReader fileReader;
        int numLinies;
        int numCaracters;
        BufferedReader bufferedReader = null;
        String linia;

        arxius.add("a.txt");
        arxius.add("b.txt");

        try {
            for (String arxiu : arxius) {
                numLinies = 0;
                numCaracters = 0;

                fileReader = new FileReader(arxiu);
                bufferedReader = new BufferedReader(fileReader);

                while ((linia = bufferedReader.readLine()) != null) {
                    numLinies++;
                    numCaracters += linia.length();
                }
                System.out.println("L'arxiu " + arxiu + " té " + numLinies + " linies i " + numCaracters + " caracters." );
            }
        } catch (FileNotFoundException e) {
            System.out.println("Excepció provocada pel \"new FileReader(arxiu)\".");
            e.printStackTrace();
        } catch (IOException e) {
        System.out.println("Excepció provocada pel \"buffer.readLine()\".");
        e.printStackTrace();
        } finally {
            try {
                bufferedReader.close();
            } catch (IOException e) {
                System.out.println("Excepció provocada pel \"buffer.close()\".");
                e.printStackTrace();
            }
        }
    }

    public static void menu4() throws IOException {
        TreeMap<Character, Integer> mapaOrdenat = new TreeMap<Character, Integer>();
        String arxiu = "a.txt";
        FileReader fileReader = null;
        int llegit;
        int totalCaractersLlegits;
        FileWriter fileWriter = null;

        try {
            fileReader = new FileReader(new File(arxiu));
            llegit = -1;
            totalCaractersLlegits = 0;
            while ((llegit = fileReader.read()) > 0) {
                if (mapaOrdenat.containsKey((char)llegit)) {
                    mapaOrdenat.put((char)llegit, mapaOrdenat.get((char)llegit) + 1);
                } else {
                    mapaOrdenat.put((char)llegit, 1);
                }
                totalCaractersLlegits++;
            }

            escriureResultatsAlArxiu(mapaOrdenat, totalCaractersLlegits);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fileReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void escriureResultatsAlArxiu(TreeMap<Character, Integer> mapaOrdenat, int totalCaractersLlegits) throws IOException{
        FileWriter fileWriter = null;

        try {
            fileWriter = new FileWriter(new File("menu4_resultat.txt"));

            for(Map.Entry<Character, Integer> dada : mapaOrdenat.entrySet()) {
                fileWriter.write("Caràcter: " + dada.getKey() +
                        ", repeticions: " + dada.getValue() +
                        ", percentatge: " + dada.getValue() * 100 / totalCaractersLlegits + "% \n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            fileWriter.close();
        }
    }

    public static void menu5() throws IOException {
        String link = "http://www.escoladeltreball.org/ca";
        BufferedReader bufferReader = null;
        boolean append = false;
        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        try {
            URL url = new URL(link);
            URLConnection connection = url.openConnection();
            bufferReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            fileWriter = new FileWriter("menu5_links.txt", append);
            printWriter = new PrintWriter(fileWriter, true);

            Pattern p = Pattern.compile("(<a href=\")([^\"]+)(\")");
            String line = "";
            while ((line = bufferReader.readLine()) != null) {
                Matcher matcher = p.matcher(line);
                while (matcher.find()) {
                    fileWriter.write(matcher.group(2) + "\n");
                    printWriter.println(matcher.group(2));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                bufferReader.close();
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            printWriter.close();
        }
    }

    public static void menu10() {
        File directori = new File("directori");
        List<String> llistaContingutDirectori = new ArrayList<String>();
        mostrarFitxersIDirectoris(directori, llistaContingutDirectori);

        System.out.println("Contingut del directori:");
        for (String dada : llistaContingutDirectori) {
            System.out.println(dada);
        }
    }

    public static void mostrarFitxersIDirectoris(File directori, List<String> llistaContingutDirectori) {
        String pathDirectoriActual;
        String[] contingutDirectoriActual;
        pathDirectoriActual = directori.getPath();
        contingutDirectoriActual = directori.list();

        for (int i = 0; i < contingutDirectoriActual.length; i++) {
            File f = new File(pathDirectoriActual + "/" + contingutDirectoriActual[i]);
            if (f.isDirectory()) {
                mostrarFitxersIDirectoris(f, llistaContingutDirectori);
            }
            llistaContingutDirectori.add(contingutDirectoriActual[i]);
        }
    }

    public static void menu11() {
        String nomDirectoriBackup = "backUp";
        String pathOrigen = "directori";
        File [] arrayDeFilesDelOrigen;
        int numBackup;
        File backupFile;
        String pathDesti;

        arrayDeFilesDelOrigen = new File(pathOrigen).listFiles();
        numBackup = trobarNumBackupMesAlt(arrayDeFilesDelOrigen);
        numBackup += 1;
        backupFile = new File(pathOrigen + "/" + nomDirectoriBackup + numBackup);
        backupFile.mkdir();
        pathDesti = pathOrigen + "/" + nomDirectoriBackup + numBackup;

        copiarTotsArxiusAutomaticament(backupFile, arrayDeFilesDelOrigen);

        System.out.println("FI");
    }

    public static int trobarNumBackupMesAlt(File[] filesDelDirectori) {
        int numBackupMesGran;
        String nomArxiu;
        String numBackUpTrobat;
        numBackupMesGran = 0;
        for (File f : filesDelDirectori) {
            if (f.isDirectory()) {
                nomArxiu = f.getName();
                if (nomArxiu.matches(".*backUp[0-9]+")){
                    numBackUpTrobat = nomArxiu.substring(6, nomArxiu.length());
                    if (numBackupMesGran < Integer.parseInt(numBackUpTrobat)){
                        numBackupMesGran = Integer.parseInt(numBackUpTrobat);
                    }
                }
            }
        }
        return numBackupMesGran;
    }

    public static void copiarTotsArxiusAutomaticament(File backupFile, File[] arrayDeFilesDelOrigen) {
        for (File fileOrigen : arrayDeFilesDelOrigen) {
            if (fileOrigen.isFile()) {
                if (fileOrigen.canRead()) {
                    try {
                        FileUtils.copyFileToDirectory(fileOrigen, backupFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    System.out.println("L'arxiu "+ fileOrigen +" no s'ha pogut copiar ja que no té permisos de lectura.");
                }
            }
        }
    }

    public static void menu12() {
        String nomDirectoriBackup="backUp";
        String pathOrigen = "directori";
        File [] arrayDeFilesDelOrigen;
        int numBackup;
        File backupFile;
        String pathDesti;


        arrayDeFilesDelOrigen = new File(pathOrigen).listFiles();

        numBackup = trobarNumBackupMesAlt(arrayDeFilesDelOrigen);
        numBackup = numBackup + 1;
        backupFile = new File (pathOrigen + "/" + nomDirectoriBackup + numBackup);
        backupFile.mkdir();
        pathDesti = pathOrigen + "/" + nomDirectoriBackup + numBackup;

        copiarTotsArxiusManualment(pathDesti, arrayDeFilesDelOrigen);

        System.out.println("FI");
    }

    public static void copiarTotsArxiusManualment(String pathDesti, File [] arrayDeFilesDelOrigen) {
        for (File fOrigen : arrayDeFilesDelOrigen) {
            File fDesti = new File(pathDesti + "/" + fOrigen.getName());
            if (fOrigen.canRead()) {
                try {
                    fDesti.createNewFile();
                    copiarContingutFitxer(fOrigen, fDesti);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("L'arxiu "+ fOrigen +" no s'ha pogut copiar ja que no té permisos de lectura.");
            }
        }
    }

    private static void copiarContingutFitxer(File fOrigen, File fDesti) throws IOException {
        FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;
        boolean append = true;
        int numCaractersLLegits;
        byte buff[] = new byte[100];

        fileInputStream = new FileInputStream(fOrigen);
        fileOutputStream = new FileOutputStream(fDesti, append);

        try {
            while ((numCaractersLLegits = fileInputStream.read(buff, 0, 100))>-1) {
                fileOutputStream.write(buff, 0, numCaractersLLegits);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            fileOutputStream.close();
            fileInputStream.close();
        }
    }

    public static passarMultiplesParametres menu20() {
        String path = null;
        File directoriArrel = null;
        File directoriOrigen = null;
        List<File> llistaFitxers = null;
        passarMultiplesParametres objPassarMultiplesParametres;

        try {
            path = "directoriMenu20/";
            directoriArrel = new File(path);
            FileUtils.forceMkdir(directoriArrel);
            directoriOrigen = new File(path + "origen");
            FileUtils.forceMkdir(directoriOrigen);
            System.out.println("APARTAT 1: Creats els directoris " + directoriArrel.getPath() + " i " + directoriOrigen.getPath());
            System.out.println();

            String nomFitxer;
            boolean fitxerCreat = false;
            ArrayList<String> totsNomsFitxers = new ArrayList<String>();
            for (int i = 0; i < 6; i++) {
                nomFitxer = path + "fitxer" + i + ".txt";
                totsNomsFitxers.add(nomFitxer);
                File fitxerTmp = new File(nomFitxer);
                fitxerCreat = fitxerTmp.createNewFile();
                System.out.println("APARTAT 2: S'ha creat el fitxer " + nomFitxer + "? " + fitxerCreat);
            }

            System.out.println();

            String[] extensionsFitxers = {"txt", "java","xml"};
            llistaFitxers = (List<File>)FileUtils.listFiles(directoriArrel, extensionsFitxers, true);

            for (File fitxer : llistaFitxers) {
                FileUtils.writeLines(fitxer, "UTF-8", totsNomsFitxers, null, true);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("APARTAT 3: ARA ES HORA DE MIRAR SI S'HA AFEGIT EL CONTINGUT ALS FITXERS QUE ENCARA ES TROBEN EN " + path);

        objPassarMultiplesParametres = new passarMultiplesParametres(llistaFitxers, directoriOrigen, directoriArrel, path);

        return objPassarMultiplesParametres;
    }

    public static passarMultiplesParametres menu21(passarMultiplesParametres objPassarMultiplesParametres) {
        try {
            Iterator<File> it = objPassarMultiplesParametres.getLlistaFitxers().iterator();
            while (it.hasNext()){
                FileUtils.moveFileToDirectory(it.next(), objPassarMultiplesParametres.getDirectoriOrigen(), true);
            }
            System.out.println("APARTAT 4: S'HAN MOGUT ELS 5 FITXERS A " + objPassarMultiplesParametres.getDirectoriOrigen().getPath());
            System.out.println();

            File fitxer3txt = FileUtils.getFile(objPassarMultiplesParametres.getDirectoriOrigen(), "fitxer3.txt");
            System.out.println("APARTAT 5: Està fitxer3.txt?: " + FileUtils.directoryContains(objPassarMultiplesParametres.getDirectoriOrigen(), fitxer3txt));
            System.out.println();

            FileUtils.forceDelete(fitxer3txt);
            System.out.println("APARTAT 6: Està fitxer3.txt?: " + FileUtils.directoryContains(objPassarMultiplesParametres.getDirectoriOrigen(), fitxer3txt));
            System.out.println();

            File fitxer1txt = FileUtils.getFile(objPassarMultiplesParametres.getDirectoriOrigen(), "fitxer1.txt");
            File fitxer5txt = FileUtils.getFile(objPassarMultiplesParametres.getDirectoriOrigen(), "fitxer5.txt");
            System.out.println("APARTAT 7:");
            System.out.println("Data (long Date) última modificació de fitxer1.txt: " + fitxer1txt.lastModified());
            System.out.println("Data (LocalDateTime) última modificació de fitxer1.txt: " + Data.imprimirLongTime(fitxer1txt.lastModified()));
            System.out.println("Data (long Date) última modificació de fitxer5.txt: " + fitxer5txt.lastModified());
            System.out.println("Data (LocalDateTime) última modificació de fitxer5.txt: " + Data.imprimirLongTime(fitxer5txt.lastModified()));
            System.out.println("Fitxer1.txt és meś vell que fitxer5.txt?: " + FileUtils.isFileOlder(fitxer1txt, fitxer5txt));
            System.out.println();

            FileUtils.writeStringToFile(fitxer1txt, "linia sobreescrita", "UTF-8", false);
            System.out.println("APARTAT 8: CANVIAT EL CONTINGUT DEL FITXER fitxer1.txt");
            System.out.println();


            System.out.println("APARTAT 9:");
            System.out.println("Data (long Date) última modificació de fitxer1.txt: " + fitxer1txt.lastModified());
            System.out.println("Data (Calendar) última modificació de fitxer1.txt: " + Data.imprimirLongTime(fitxer1txt.lastModified()));
            System.out.println("Data (long Date) última modificació de fitxer5.txt: " + fitxer5txt.lastModified());
            System.out.println("Data (Calendar) última modificació de fitxer5.txt: " + Data.imprimirLongTime(fitxer5txt.lastModified()));
            System.out.println("Fitxer1.txt és meś nou que fitxer5.txt?: " + FileUtils.isFileNewer(fitxer1txt, fitxer5txt));
            System.out.println();

            FileUtils.touch(fitxer5txt);
            System.out.println("APARTAT 10");
            System.out.println("Data (long Date) última modificació de fitxer1.txt: " + fitxer1txt.lastModified());
            System.out.println("Data (Calendar) última modificació de fitxer1.txt: " + Data.imprimirLongTime(fitxer1txt.lastModified()));
            System.out.println("Data (long Date) última modificació de fitxer5.txt: " + fitxer5txt.lastModified());
            System.out.println("Data (Calendar) última modificació de fitxer5.txt: " + Data.imprimirLongTime(fitxer5txt.lastModified()));
            System.out.println("Fitxer1.txt és meś nou que fitxer5.txt?: " + FileUtils.isFileNewer(fitxer1txt, fitxer5txt));
            System.out.println();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return objPassarMultiplesParametres;
    }

    public static void menu22(passarMultiplesParametres objPassarMultiplesParametres) {
        try {
            File directoriDesti = new File(objPassarMultiplesParametres.getPath() + "desti");
            FileUtils.moveDirectoryToDirectory(objPassarMultiplesParametres.getDirectoriOrigen(), directoriDesti, true);
            System.out.println("APARTAT 11: S'HA MOGUT EL DIRECTORI " + objPassarMultiplesParametres.getDirectoriOrigen().getPath() + " A DINS DEL DIRECTORI " + directoriDesti.getPath());
            System.out.println();

            System.out.println("APARTAT 12: EL TAMANY DEL DIRECTORI desti ES: " + FileUtils.sizeOfDirectoryAsBigInteger(directoriDesti));
            System.out.println("SUMEU EL TAMANY DELS 5 FITXERS QUE HI HA DINS DEL DIRECTORI desti I VEUREU QUE"
                    + "\n SUMEN 438 BYTES SEMPRE I QUAN NOMÉS EXECUTEU ELS MENÚS 20 I 21 UNA VEGADA.");
            System.out.println();

            System.out.println("APARTAT 13:");
            Iterator it2 = FileUtils.iterateFilesAndDirs(objPassarMultiplesParametres.getDirectoriArrel(), new WildcardFileFilter("*.txt"), new WildcardFileFilter("*"));
            while (it2.hasNext()) {
                File fileTmp = (File) it2.next();
                System.out.println("    NOM DEL FILE TROBAT: " + ((File) fileTmp).getName());
                System.out.println("    PATH DEL FILE TROBAT: " + ((File) fileTmp).getPath());
                System.out.println("    ES UN FITXER?: " + ((File) fileTmp).isFile());
                System.out.println("    ES UN DIRECTORI?: " + ((File) fileTmp).isDirectory());
                System.out.println();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void menu30() throws IOException {
        LocalDateTime today;
        ArrayList<Naus_Dades> arrayListDeNaus = new ArrayList<Naus_Dades>();
        File fitxer = new File("Naus_Dades.dat");
        RandomAccessFile fitxerEmmagatzematge = null;
        int longString;
        StringBuffer stringBuffer = null;

        today = LocalDateTime.now();
        arrayListDeNaus.add(new Naus_Dades("Agamemnon", "destructor", "Omega".toCharArray(), today, "Enviat a la flota de Babylon 5"));
        arrayListDeNaus.add(new Naus_Dades("Achilles", "destructor", "Omega".toCharArray(), today, "Enviat a la flota de Babylon 5"));
        arrayListDeNaus.add(new Naus_Dades("Cortez", "explorador", "Explorer".toCharArray(), today, "Enviat a l'espai profund"));

        try {
            fitxerEmmagatzematge = new RandomAccessFile(fitxer, "rw");
            for (Naus_Dades nauTmp : arrayListDeNaus) {
                //Nom
                longString = nauTmp.getNom().length();
                fitxerEmmagatzematge.writeInt(longString);
                fitxerEmmagatzematge.writeChars(nauTmp.getNom());

                //Tipus
                longString = nauTmp.getTipus().length();
                fitxerEmmagatzematge.writeInt(longString);
                fitxerEmmagatzematge.writeChars(nauTmp.getTipus());

                //Model
                stringBuffer = new StringBuffer(String.valueOf(nauTmp.getModel()));
                stringBuffer.setLength(50);
                fitxerEmmagatzematge.writeChars(stringBuffer.toString());

                //Data construcció
                fitxerEmmagatzematge.writeChars(String.valueOf(nauTmp.getDataConstruccio()));

                //Descripció
                longString = nauTmp.getDescripcio().length();
                fitxerEmmagatzematge.writeInt(longString);
                fitxerEmmagatzematge.writeChars(nauTmp.getDescripcio());
                System.out.println("Gravada la nau " + nauTmp.getNom());
            }
            System.out.println("S'han gravat totes les naus amb èxit.");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            fitxerEmmagatzematge.close();
            System.out.println("Fitxer tancat amb èxit.");
        }
    }

    public static void menu31() throws IOException {
        File fitxer = new File("Naus_Dades.dat");
        RandomAccessFile fitxerEmmagatzematge = null;
        String stringTmp;
        int longString;


        fitxerEmmagatzematge = new RandomAccessFile(fitxer, "r");

        while (fitxerEmmagatzematge.getFilePointer() != fitxerEmmagatzematge.length()) {
            System.out.println("----------------");

            stringTmp = "";
            longString = fitxerEmmagatzematge.readInt();
            System.out.println("Nom.length(): " + longString);
            for(int i = 0; i< longString; i++) {
                stringTmp = stringTmp + fitxerEmmagatzematge.readChar();
            }
            System.out.println("Nom: " + stringTmp);

            stringTmp = "";
            longString = fitxerEmmagatzematge.readInt();
            System.out.println("tipus.length(): " + longString);
            for(int i = 0; i< longString; i++) {
                stringTmp = stringTmp + fitxerEmmagatzematge.readChar();
            }
            System.out.println("Tipus: " + stringTmp);

            stringTmp = "";
            for(int i = 0; i< 50; i++) {
                stringTmp = stringTmp + fitxerEmmagatzematge.readChar();
            }
            System.out.println("Model: " + stringTmp);

            stringTmp = "";
            for(int i = 0; i< 26; i++) {
                stringTmp = stringTmp + fitxerEmmagatzematge.readChar();
            }
            System.out.println("Data construcció: " + Data.imprimirData(LocalDateTime.parse(stringTmp)));

            stringTmp = "";
            longString = fitxerEmmagatzematge.readInt();
            System.out.println("descripcio.length(): " + longString);
            for(int i = 0; i< longString; i++) {
                stringTmp = stringTmp + fitxerEmmagatzematge.readChar();
            }
            System.out.println("Descripció: " + stringTmp);
        }

        fitxerEmmagatzematge.close();

        System.out.println();
        System.out.println("Fitxer llegit i tancat amb èxit.");
    }
}
