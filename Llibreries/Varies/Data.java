package Varies;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;

public abstract class Data {
	public static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

	
	public static String imprimirData(LocalDateTime dataTmp) {
		if (dataTmp == null) {
        	return "NULL";
        } else {
        	return dataTmp.format(formatter);
        }
	}

	public static LocalDateTime imprimirLongTime(long time) {
		time = 1499070300000L;
		LocalDateTime triggerTime =
				LocalDateTime.ofInstant(Instant.ofEpochMilli(time),
						TimeZone.getDefault().toZoneId());
		return triggerTime;
	}
}
